precision mediump float;

uniform float time;
varying vec2 vTextureCoord;
uniform sampler2D uSampler;
uniform vec2 resolution;
uniform float tileSize;

// flashing filter for the documents sprite

void main(void) {
	vec4 texColor = texture2D(uSampler, vTextureCoord);
	
	// adju not used currently but could be
	// so that the effect is also moving at the x axis
	float adju = vTextureCoord.x*resolution.x/tileSize;
	float adjv = vTextureCoord.y*resolution.y/tileSize;

	// some coefficient that concetrates on specific regions of the sprite
	// the extrema of the cosine
	// all of them are >0 because of the even power
	// phase shift via the time uniform
	float coef = 0.6*pow(cos(4.0*adjv-4.0*time),8.0);
	// increase the color only where there is the actual sprite (alpha not zero)
	if (texColor.w>0.01) {
		texColor.x += coef;
		texColor.y += coef;
		texColor.z += coef;
	}

	gl_FragColor = texColor;
}