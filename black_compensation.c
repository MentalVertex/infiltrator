precision mediump float;

varying vec2 vTextureCoord;
uniform sampler2D uSampler;
uniform float darkness;
uniform float mouse_over;

// filter to up the brightness for the UI sprites
// to counter the level darkness

void main(void) {
	gl_FragColor = texture2D(uSampler, vTextureCoord);
	// experimental alpha to have as few "sprite leftovers" as possible
	if (gl_FragColor.a > 0.2) {
		gl_FragColor += darkness;
	}
}