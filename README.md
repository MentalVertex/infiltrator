2D stealth game, where you have to steal the secret documents without being detected by the guards.

<a href="https://totenarctanz.itch.io/infiltrator">Play on itch.io </a>

The game was made using Phaser 2.7.5 (not included).

![inf.webm](vid/inf.webm)