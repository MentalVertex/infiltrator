'use strict';

var resX = 512;
var resY = 512;
var game = new Phaser.Game(resX, resY, Phaser.AUTO, 'Infiltrator!');

// globals
var g = {
	currLevel: null,
	allLevelsData: null,
	levelData: null,
	levelBitmapData: null,
	levelSprite: null,
	lvlDarkness: null,
	tileSize: null,
	docsSprite: null,
	exitPos: null,
	fans: [],
	wallSegments: [],
	walls: [],
	wallsGroup: null,
	menuButton: null,
	restartButton: null,
	audioButton: null,
	blackCompFx: null,

	cursors: null,
	wasdr: null,

	stepSfx: null,
	fansSfx: null,
	obtainSfx: null,
	detectedSfx: null,
	successSfx: null,
	bounceSfx: null,
	bounceSfx2: null,
	birdsSfx: null,
	music: null,

	menuBackroundImg: null,
	menuBackroundImg2: null,
	menuPlayerImg: null,
	fadeFx: null,
	fadeBlackFx: null,
	fadeTime: null,
	rt1: null,
	rt2: null,
	rtc: null,
	rtSpr: null,
	logoSpr: null,
	logoVel: null,
	playSpr: null,
	playVel: null,

	player: null,
	detected: null,
	gotDocs: null,
	atExit: null,
	enemyGroup: null,
	enemies: [],
	enFXSpr: [],
	fovGfx: [],
	enemyNextTiles: [],

	levelReached: null
};

var loadState = {
	preload: function(){
		game.load.shader('enemyFovShader', 'enemy_fov_filter.c');
		game.load.shader('docsShader', 'docs_filter.c');
		game.load.shader('fadeShader', 'fade_filter.c');
		game.load.shader('fadeBlackShader', 'fade_black_filter.c');
		game.load.shader('blackCompensationShader', 'black_compensation.c');

	    game.load.image('ground_tile', 'assets/img/ground_tile.png');
	    game.load.image('hole_tile', 'assets/img/hole.png');
	    game.load.image('wall_tile', 'assets/img/wall_tile.png');
	    game.load.image('fan_tile', 'assets/img/fan_tile.png');
	    game.load.image('docs_tile', 'assets/img/documents.png');
	    game.load.image('menu_back_img', 'assets/img/menu_background.png');
	    game.load.image('logo_img', 'assets/img/logo.png');
	    game.load.image('play_img', 'assets/img/play.png');
	    game.load.image('level_orb_img', 'assets/img/level_orb.png');
	    game.load.image('level_orb_na_img', 'assets/img/level_orb_na.png');
	    game.load.image('choose_lvl_img', 'assets/img/choose_level.png');
	    game.load.image('level_no_img', 'assets/img/level_no.png');
	    game.load.image('menu_button_img', 'assets/img/menu_button.png');
	    game.load.image('restart_img', 'assets/img/restart.png');
	    game.load.image('last_level_img', 'assets/img/last_level.png');
	    game.load.image('the_end_img', 'assets/img/the_end.png');
	    game.load.image('play_again_img', 'assets/img/play_again.png');

	    game.load.spritesheet('player', 'assets/img/player.png', 64, 64);
	    game.load.spritesheet('enemy', 'assets/img/enemy.png', 64, 64);
	    game.load.spritesheet('numbers', 'assets/img/numbers.png', 64, 32);
	    game.load.spritesheet('numbers_na', 'assets/img/numbers_na.png', 64, 32);
	    game.load.spritesheet('audio_toggle', 'assets/img/audio_toggle.png', 32, 32);

	    game.load.audio('step_sfx', ['assets/audio/step.ogg', 'assets/audio/step.mp3']);
	    game.load.audio('obtain_sfx', ['assets/audio/obtain.ogg', 'assets/audio/obtain.mp3']);
	    game.load.audio('fans_sfx', ['assets/audio/fans.ogg', 'assets/audio/fans.mp3']);
	    game.load.audio('detected_sfx', ['assets/audio/alarm.ogg', 'assets/audio/alarm.mp3']);
	    game.load.audio('bounce_sfx', ['assets/audio/bounce.ogg', 'assets/audio/bounce.mp3']);
	    game.load.audio('birds_sfx', ['assets/audio/birds_effects.ogg', 'assets/audio/birds_effects.mp3']);
	    game.load.audio('music', ['assets/audio/music.ogg', 'assets/audio/music.mp3']);

	    game.load.text('level_data', 'assets/levels.txt');
	},

	create: function(){
		g.currLevel = 0;
		g.stepSfx = game.add.audio('step_sfx');
		g.bounceSfx = game.add.audio('bounce_sfx');
		g.bounceSfx2 = game.add.audio('bounce_sfx');
		g.birdsSfx = game.add.audio('birds_sfx');
		g.music = game.add.audio('music');
		
		g.music.volume = 0.0;
		g.music.loop = true;

		var levelTextData = game.cache.getText('level_data');
	    g.allLevelsData = JSON.parse(levelTextData);

	    var blackCompUniforms = {darkness: {type: '1f', value:0}};
	    g.blackCompFx = new Phaser.Filter(game, blackCompUniforms, game.cache.getShader('blackCompensationShader'));

		// have all levels unlocked
	    g.levelReached = 23;
	    // g.levelReached = 0;

		/* uncomment to used local storage to lock levels 
	    if (localStorage.getItem('infiltrator_level_progress')){
	    	g.levelReached = JSON.parse(localStorage.getItem('infiltrator_level_progress'));
	    } else {
	    	localStorage.setItem('infiltrator_level_progress', JSON.stringify(g.levelReached));
	    }
		*/

		game.world.removeAll();
		game.state.start('menuAnim');
	}
}


var menuAnimState = {
	create: function() {
		// the whole background is 2 scrolling indentical sprites, one next to the other
		g.menuBackroundImg = game.add.sprite(0, 0, 'menu_back_img');
		g.menuBackroundImg2 = game.add.sprite(512, 0, 'menu_back_img');

		// running player animation init
		g.menuPlayerImg = game.add.sprite(256, 256, 'player');
		// by setting the anchor to 0.5, the sprite's position and center of rotation is at its center
		g.menuPlayerImg.anchor.setTo(0.5, 0.5);
	    g.menuPlayerImg.scale.setTo(0.5, 0.5);
	    g.menuPlayerImg.angle = 90.0;
		g.menuPlayerImg.animations.add('anim', [0, 1, 2, 3, 4], 15, true);
		g.menuPlayerImg.animations.play('anim', 15, true, false);

		// add two rendertextures to swap between for the blur effect
	 	g.rt1 = game.add.renderTexture(512, 512, 'rt1');
		 g.rt2 = game.add.renderTexture(512, 512, 'rt2');
		// reference to the current rendertexture
	 	g.rtc = g.rt1;

		// the resulting sprite from the rendertexture
	 	g.rtSpr = game.add.sprite(0, 0, g.rtc);

		// the "infiltrator" and "play" sprites
		// initial speed = 0. then they start to fall
	 	g.logoSpr = game.add.sprite(256, -128, 'logo_img');
		g.logoSpr.anchor.x = 0.5;
		g.logoSpr.anchor.y = 0.5;
	 	g.logoVel = 0;
	 	g.playSpr = game.add.sprite(256, -128, 'play_img');
	 	g.playSpr.anchor.x = 0.5;
	 	g.playSpr.anchor.y = 0.5;
	 	g.playVel = 0;

		// time of blur animation
		g.fadeTime = 0.0;

		g.lvlDarkness = 1.0;

		// setup of the blur shader
		var fadeUniforms = {fade_time: {type: '1f', value: g.fadeTime},};
		 g.fadeFx = new Phaser.Filter(game, fadeUniforms, game.cache.getShader('fadeShader'));
		// setup of the fading shader at lose/win state
		var fadeBlackUniforms = {fade_time: {type: '1f', value: g.fadeTime},
								 darkness: {type: '1f', value: g.lvlDarkness}};
	 	g.fadeBlackFx = new Phaser.Filter(game, fadeBlackUniforms, game.cache.getShader('fadeBlackShader'));

		// add the fade effect to the rendertexture
		g.rtSpr.filters = [g.fadeFx];

		// addd the fade to black/white effect to the whole render area
		game.world.filters = [g.fadeBlackFx];

		g.audioButton = game.add.sprite(0, 0, 'audio_toggle');
		g.audioButton.frame = game.sound.mute ? 1 : 0;
		g.audioButton.alpha = 0.4;
	    g.audioButton.inputEnabled = true;
		g.audioButton.events.onInputOver.add(function(){g.audioButton.alpha = 0.8;}, this);
		g.audioButton.events.onInputOut.add(function(){g.audioButton.alpha = 0.4;}, this);
		g.audioButton.events.onInputDown.add(function(){
			game.sound.mute = !game.sound.mute;
			// switch between the on/off speaker sprites
			g.audioButton.frame = game.sound.mute ? 1 : 0;
		}, this);

		// keep music playing during the game, it will be turned on/off via volume
		g.music.play();
	},

	
	update: function() {
		// intro music fade in
		// linear, maybe needs gamma correction
		if (g.music.volume<1) g.music.volume += 0.0025;

		// keep scrolling the images until the right image reaches x = 0 (left of screen)
		// then the animation ends and the blur/fade effect starts
		if (g.menuBackroundImg2.x>0){
			// keep brightening up the animation and send uniform to shader 
			if (g.lvlDarkness>0.0) g.lvlDarkness -= 0.02;
			g.fadeBlackFx.uniforms.darkness.value = g.lvlDarkness;
			g.menuBackroundImg.x -= 2;
			g.menuBackroundImg2.x -= 2;
			g.stepSfx.play('', 0, 0.8, false, false);
		} else {
			// stop scrolling, start fading
			g.fadeTime += 0.01;
			if (g.fadeTime < 1.7){
				// stop player animation
				g.menuPlayerImg.frame = 1;
				// update blur filter uniforms
				g.rtSpr.filters[0].uniforms.fade_time.value = g.fadeTime;
				// swap render textures
				// while one is being drawn, the other has the blur filter applied
				// then they swapplaces and repeat
				var temp = g.rt1;
				g.rt1 = g.rt2;
				g.rt2 = temp;

				// update rendertexture sprite with the current rendertexture
				g.rtSpr.setTexture(g.rt1);

				// render the current world to texture
				g.rt2.renderXY(game.world,0,0,false);
			} else {
				// begin logo bouncing
				// constant velocity increase -> accelerated fall
				g.logoVel += 0.1;
				g.logoSpr.y += g.logoVel;
				g.playVel += 0.1;
				g.playSpr.y += g.playVel;
				// hitting the ground "compresses" the sprite (see below), so start restoring it
				if (g.logoSpr.scale.y < 1) g.logoSpr.scale.y += 0.05;
				// ground touched, now bounce
				if (g.logoSpr.y > 192) {
					if (g.logoVel<0.2) g.logoVel = 0;
					// deform effect, relative to the fall speed
					g.logoSpr.scale.y -= 0.07*g.logoVel;
					// some volume calculation based on speed
					var vol = g.logoVel*g.logoVel/10;
					// clamp bounce sound volume to 0.6
					vol = vol > 0.6 ? 0.6 : vol;
					g.bounceSfx.play('', 0, vol, false, true);
					// speed damping
					g.logoVel *= -0.5;
					// make sure >192 is not true anymore
					g.logoSpr.y = 192;
				}
				// the same for the PLAY button
				if (g.playSpr.scale.y < 1) g.playSpr.scale.y += 0.05;
				if (g.playSpr.y > 320){
					if (g.playVel<0.2) g.playVel = 0;
					g.playSpr.scale.y -= 0.07*g.playVel;
					var vol2 = g.playVel*g.playVel/10;
					vol2 = vol2 > 0.6 ? 0.6 : vol2;
					g.bounceSfx2.play('', 0, vol2, false, true);
					g.playVel *= -0.5;
					g.playSpr.y = 320;
				}
				// intro animation ended, enable controls
				if (g.fadeTime > 4.5) {
					g.playSpr.inputEnabled = true;
					g.playSpr.events.onInputOver.add(this.SprScaleUp, this, 0, g.playSpr);
					g.playSpr.events.onInputOut.add(this.SprScaleDown, this, 0, g.playSpr);
					g.playSpr.events.onInputDown.addOnce(function()
						{
							// clear scene, get ready for the menu screen
							game.world.filters = [];
							game.world.removeAll();
							game.state.start('menu');
						}, this);
				}
			}
		}
	},

	SprScaleUp: function(spr){
		spr.scale.x = 1.2;
		spr.scale.y = 1.2;
	},
	SprScaleDown: function(spr){
		spr.scale.x = 1.0;
		spr.scale.y = 1.0;
	},
}

var menuState = {
	create: function(){
		// add the previously rendered (blurred) texture as a menu background
		game.add.sprite(0, 0, g.rtc);

		g.audioButton = game.add.sprite(16, 16, 'audio_toggle');
		g.audioButton.frame = game.sound.mute ? 1 : 0;		
		g.audioButton.anchor.x = 0.5;
		g.audioButton.anchor.y = 0.5;
		g.audioButton.scale.x = 1.0;
		g.audioButton.scale.y = 1.0;
	    g.audioButton.inputEnabled = true;
		g.audioButton.events.onInputOver.add(function(){g.audioButton.scale = {x:1.2, y:1.2};}, this);
		g.audioButton.events.onInputOut.add(function(){g.audioButton.scale = {x:1.0, y:1.0};}, this);
		g.audioButton.events.onInputDown.add(function(){
			game.sound.mute = !game.sound.mute;
			g.audioButton.frame = game.sound.mute ? 1 : 0;
		}, this);

		// level select sprites and buttons
		// 8x3 array for the 24 levels
		// maybe needs more modular approach if more levels are to be added
		for (var i=0; i<3; i++) {
			for (var j=0; j<8; j++){
				var tempOrb;
				var tempNumber;
				var linearIdx = i*8 + j;

				// for not yet reached levels use the red sprite, otherwise the blue one
				if (linearIdx <= g.levelReached) {
					tempOrb = game.add.sprite(j*64, 160+i*64, 'level_orb_img');
					tempNumber = game.add.sprite(j*64+32, 48+144+i*64, 'numbers');
				} else {
					tempOrb = game.add.sprite(j*64, 160+i*64, 'level_orb_na_img');
					tempNumber = game.add.sprite(j*64+32, 48+144+i*64, 'numbers_na');
				}

				tempNumber.anchor.x = 0.5;
				tempNumber.anchor.y = 0.5;
				// index into the 24 frames array to get the correctly numbered sprite
				tempNumber.frame = linearIdx;
				tempNumber.inputEnabled = true;
				tempNumber.events.onInputOver.add(this.SprScaleUp, this, 0, tempNumber);
				tempNumber.events.onInputOut.add(this.SprScaleDown, this, 0, tempNumber);
				// the clicked level is available, enter it
				if (linearIdx <= g.levelReached) tempNumber.events.onInputDown.add(this.loadGame, this, 0, tempNumber);
			}
		}

		game.add.sprite(128, 64, 'choose_lvl_img');
	},

	loadGame: function(levelNum){
		// load the level depending on which sprite was clicked
		g.currLevel = levelNum.frame;
		game.world.removeAll();
		game.state.start('game');
	},

	SprScaleUp: function(spr){
		spr.scale.x = 1.1;
		spr.scale.y = 1.1;
	},
	SprScaleDown: function(spr){
		spr.scale.x = 1.0;
		spr.scale.y = 1.0;
	},
}


var gameState = {
	preload: function() {
	    g.levelData = g.allLevelsData[g.currLevel];
	},

	setupGUI: function(){
		g.audioButton = game.add.sprite(0, 0, 'audio_toggle');
		g.audioButton.frame = game.sound.mute ? 1 : 0;		
		g.audioButton.alpha = 0.4;
		// when the whole screen is dark in dark levels,
		// this filter compensates by lightening up the GUI sprites
		// it's a really clunky and naive method but the result is ok
		g.audioButton.filters = [g.blackCompFx];
	    g.audioButton.inputEnabled = true;
		g.audioButton.events.onInputOver.add(function(){g.audioButton.alpha = 0.8;}, this);
		g.audioButton.events.onInputOut.add(function(){g.audioButton.alpha = 0.4;}, this);
		g.audioButton.events.onInputDown.add(function(){
			game.sound.mute = !game.sound.mute;
			g.audioButton.frame = game.sound.mute ? 1 : 0;
		}, this);

	    var levelNumber1 = game.add.sprite(256-64, 0, 'level_no_img');
	    var levelNumber2 = game.add.sprite(256+32, 0, 'numbers');
	    levelNumber1.alpha = 0.4;
	    levelNumber2.alpha = 0.4;
	    levelNumber2.frame = g.currLevel;
	    levelNumber1.filters = [g.blackCompFx];
	    levelNumber2.filters = [g.blackCompFx];

	    g.menuButton = game.add.sprite(0, 480, 'menu_button_img');
	    g.menuButton.filters = [g.blackCompFx];
	    g.menuButton.alpha = 0.4;
	    g.menuButton.inputEnabled = true;
	    g.menuButton.events.onInputOver.add(function(){g.menuButton.alpha = 0.8;}, this);
	    g.menuButton.events.onInputOut.add(function(){g.menuButton.alpha = 0.4;}, this);
	    g.menuButton.events.onInputDown.add(function()
	    	{
				// restore the fade to black filter, stop sounds and return to menu 
	    		g.fadeTime = 0; 
	    		g.fadeBlackFx.uniforms.fade_time.value = g.fadeTime;
	    		g.fansSfx.stop(); 
	    		g.birdsSfx.stop();
				game.world.removeAll();
	    		game.state.start('menu');
	    	}, this);

		g.restartButton = game.add.sprite(512-32, 512-32, 'restart_img');
		g.restartButton.filters = [g.blackCompFx];
	    g.restartButton.alpha = 0.4;
		g.restartButton.inputEnabled = true;
		g.restartButton.events.onInputOver.add(function(){g.restartButton.alpha = 0.8;}, this);
	    g.restartButton.events.onInputOut.add(function(){g.restartButton.alpha = 0.4;}, this);
		g.restartButton.events.onInputDown.add(function(){game.state.restart();}, this);
	},

	setupLevel: function() {
		// apply the fade to black/white filter to the whole world
		// so GUI sprites darken to, see above
		game.world.filters = [g.fadeBlackFx];

		// the field of view effect bitmaps for all enemies
		// here we will store the shadow masks
		g.fovGfx = [];
		// enemy_paths.length = number of enemies
	    for (var i=0; i<g.levelData.enemy_paths.length; i++){
			var graphics = game.add.graphics(0,0);
			graphics.cacheAsBitmap = true;
			g.fovGfx.push(graphics);
	    }

		// empty bitmap data, we will fill this later
	    g.levelBitmapData = game.make.bitmapData(512, 512);
	    g.levelSprite = game.add.sprite(0, 0, g.levelBitmapData);

		// ??? TO BE REMOVED
	    //var area = new Phaser.Rectangle(0, 0, 64, 64);

		// if the level has at least one dimension >8 then the tilesize goes from 64->32
		// but for now all levels are 8x8 so ignore
	    var maxDim = g.levelData.width;
	    if (g.levelData.height>maxDim) maxDim = g.levelData.height;
	    g.tileSize = maxDim > 8 ? 32 : 64;

		// on the last secret level import the appropriate background
		// before adding the usual tiles
	    if (g.currLevel == 24) {
        	g.levelBitmapData.copy('last_level_img', 0, 0, 512, 512, 0, 0,
            	512, 512, 0, 0, 0, 1, 1, 1, null, false);
        }

	    // ground and hole tiles
	    for (var i=0; i<g.levelData.width; i++){
	        for (var j=0; j<g.levelData.height; j++){
				// the secret level (24) has its ground integrated into the background sprite,
				// so no ground tiles to draw
	            if (g.currLevel<24) {
	            	g.levelBitmapData.copy('ground_tile', 0, 0, 64, 64, g.tileSize*i, g.tileSize*j,
	                	g.tileSize, g.tileSize, 0, 0, 0, 1, 1, 1, null, false);
				}
				// if current tile is the entry point then add the hole sprite here
	            if (g.levelData.entry_point[0]==i && g.levelData.entry_point[1]==j){
	            	g.levelBitmapData.copy('hole_tile', 0, 0, 64, 64, g.tileSize*i, g.tileSize*j,
	                g.tileSize, g.tileSize, 0, 0, 0, 1, 1, 1, null, false);
	            }
	        }
	    }

		// group of collision-enabled tiles
	    g.wallsGroup = game.add.group();
	    g.wallsGroup.enableBody = true;

		g.walls = [];
		// individual line segments that make up the wall square/tile
	    g.wallSegments = [];
	    for (var i=0; i<g.levelData.walls.length; i++){
			// wall coordinates are 0-7. Multiplying with the tileSize gives us world coordinates
	    	var p = new Phaser.Point(g.tileSize*g.levelData.walls[i][0],g.tileSize*g.levelData.walls[i][1]);

	    	// create wall tiles at their calculated positions
			var wall = g.wallsGroup.create(p.x, p.y, 'wall_tile');

			// as we said, levels are 8x8 and tiles are 64x64 for now. Ignore
			wall.scale.setTo(g.tileSize/64, g.tileSize/64);

	        wall.body.immovable = true;
	        g.walls.push(wall);

	        // last level trees
			// we need them separately from the background 
			// so they can be physical objects to collide with (like walls of normal levels)
	        if (g.currLevel == 24){
	        	for (var j=0; j<g.levelData.trees.length; j++){
		        	var tree = g.wallsGroup.create(
						g.tileSize*g.levelData.trees[j][0], g.tileSize*g.levelData.trees[j][1], 'wall_tile');
					// ignore again
		        	tree.scale.setTo(g.tileSize/64, g.tileSize/64);
		        	tree.visible = false;
		        	tree.body.immovable = true;
		        	g.walls.push(tree);
	        	}
	        }

	        // add wall line segments for raycasting (cw for every tile)
	        g.wallSegments.push(new Phaser.Line(p.x, p.y, p.x + g.tileSize, p.y));
	        g.wallSegments.push(new Phaser.Line(p.x + g.tileSize, p.y, p.x + g.tileSize, p.y + g.tileSize));
	        g.wallSegments.push(new Phaser.Line(p.x + g.tileSize, p.y + g.tileSize, p.x, p.y + g.tileSize));
	        g.wallSegments.push(new Phaser.Line(p.x, p.y + g.tileSize, p.x, p.y));
	    }
	    // finally the box of the whole canvas
	    g.wallSegments.push(new Phaser.Line(0, 0, resX, 0));
	    g.wallSegments.push(new Phaser.Line(resX, 0, resX, resY));
	    g.wallSegments.push(new Phaser.Line(resX, resY, 0, resY));
	    g.wallSegments.push(new Phaser.Line(0, resY, 0, 0));

		// add all fan tiles
	    g.fans = [];
	    for (var i=0; i<g.levelData.fans.length; i++){
			// we need to rotate them around their centers so we need anchor at 0.5
			// but with anchor at 0.5 the placement would be off by half a tile,
			// so compensate for that
	        var fan = game.add.sprite(g.tileSize*g.levelData.fans[i][0] + g.tileSize/2,
				g.tileSize*g.levelData.fans[i][1] + g.tileSize/2, 'fan_tile');
			// ignore again
	        fan.scale.setTo(g.tileSize/64, g.tileSize/64);
	        fan.anchor.setTo(0.5, 0.5);
	        g.fans.push(fan);
	    }

		this.setupGUI();

		// documents icon on the level
		// offset them so they are at the center
	    g.docsSprite = game.add.sprite(g.tileSize*g.levelData.docs[0]+g.tileSize/4,
	    						   g.tileSize*g.levelData.docs[1]+g.tileSize/4, 'docs_tile');
	    game.physics.arcade.enable(g.docsSprite);

	    // exit hole coordinates
	    // offset a little because player sprite has position measured from top left of sprite not center
	    g.exitPos = new Phaser.Point(g.levelData.entry_point[0]*g.tileSize + g.tileSize/2 - 10,
	    						   g.levelData.entry_point[1]*g.tileSize + g.tileSize/2 - 10);		
	},

	setupPlayer: function(){
		g.player = game.add.sprite(g.exitPos.x+10, g.exitPos.y+10, 'player');
		g.player.anchor.setTo(0.5, 0.5);
	    g.player.scale.setTo(0.5*g.tileSize/64, 0.5*g.tileSize/64);
	    game.physics.arcade.enable(g.player);
	    g.player.body.setSize(42, 40, 11, 16);
	    g.player.body.collideWorldBounds = true;
	    g.player.animations.add('anim', [0, 1, 2, 3, 4], 15, true);
	    g.player.animations.add('anim_with_docs', [5, 6, 7, 8, 9], 15, true);
	    g.player.animations.add('anim_exit', [10, 11, 12, 13, 14], 15, true);
	    g.gotDocs = false;
	    g.atExit = false;
	},

	setupEnemies: function(){
		g.enemyGroup = game.add.group();

		var arr = g.levelData.enemy_paths;

		g.enemies = [];
		// the index of the next tile for each enemy path
		g.enemyNextTiles = [];
		g.enemyGroup.removeAll();
		for (var i=0; i<arr.length; i++){
			// at the beginning all enemies go to the 0th tile
		    g.enemyNextTiles.push(0);
		    var enemy = game.add.sprite(arr[i][1]*g.tileSize + g.tileSize/2, arr[i][2]*g.tileSize + g.tileSize/2, 'enemy');
		    enemy.anchor.setTo(0.5, 0.5);
		    enemy.scale.setTo(0.5*g.tileSize/64, 0.5*g.tileSize/64);
		    game.physics.arcade.enable(enemy);
		    enemy.body.setSize(42, 40, 11, 16);
			enemy.body.collideWorldBounds = true;
		    enemy.body.immovable = true;
		    enemy.animations.add('anim_enemy', [0, 1, 2, 3, 4, 3, 2, 1], 5, true);
			enemy.animations.play('anim_enemy', 10);
		    g.enemies.push(enemy);
	        g.enemyGroup.add(enemy);
		}

	},

	setupFilters: function(){
		// the resulting enemy FOV effect sprites
		g.enFXSpr = [];
	    for (var i=0; i<g.enemies.length; i++){
			var fxSpr = game.add.sprite();
			// the FOV sprites cover the whole game world
	    	fxSpr.width = 512;
	    	fxSpr.height = 512;

		    var enemyFOVUniforms = {
		    						detected: {type: '1f', value: 1.0},
		    						iChannel0: {type: 'sampler2D', value: g.fovGfx[i]._cachedSprite.texture},
		    						enemyPos: {type: '2f', value: g.enemies[i].position},
		    						enemyDist: {type: '1f', value: g.levelData.enemy_effects[i][0]},
		    						enemyFov: {type: '1f', value: g.levelData.enemy_effects[i][1]},
		    						enemyAngle: {type: '1f', value: g.enemies[i].angle}
		                           };

		    var enemyFOVFilter = new Phaser.Filter(game, enemyFOVUniforms, game.cache.getShader('enemyFovShader'));

			// the filter will cover the whole world
			// but only the relevant regions will become green/red
		    enemyFOVFilter.setResolution(resX, resY);

			// apply filter on sprite
		    fxSpr.filters = [ enemyFOVFilter ];
		    g.enFXSpr.push(fxSpr);
	    }

		// the flashing effect for the document sprite
	    var docsUniforms = {tileSize: {type:'1f', value: g.tileSize}};
	    var docsFilter = new Phaser.Filter(game, docsUniforms, game.cache.getShader('docsShader'));
	    docsFilter.setResolution(resX, resY);
	    g.docsSprite.filters = [docsFilter];

		g.lvlDarkness = g.levelData.darkness;
	    g.blackCompFx.uniforms.darkness.value = g.lvlDarkness;
		g.fadeBlackFx.uniforms.darkness.value = g.lvlDarkness;
		g.fadeTime = 0;
		g.fadeFx.uniforms.fade_time.value = g.fadeTime;
		g.fadeBlackFx.uniforms.fade_time.value = g.fadeTime;
	},

	create: function() {
		g.fansSfx = game.add.audio('fans_sfx');
		g.obtainSfx = game.add.audio('obtain_sfx');
		g.detectedSfx = game.add.audio('detected_sfx');
		g.successSfx = game.add.audio('success_sfx');

	    game.physics.startSystem(Phaser.Physics.ARCADE);

	    this.setupLevel();
	    this.setupPlayer();
	    this.setupEnemies();
	    this.setupFilters();

	    g.cursors = game.input.keyboard.createCursorKeys();
	    g.wasdr = {w: game.input.keyboard.addKey(Phaser.Keyboard.W),
	  			s: game.input.keyboard.addKey(Phaser.Keyboard.S),
	  			a: game.input.keyboard.addKey(Phaser.Keyboard.A),
	  			d: game.input.keyboard.addKey(Phaser.Keyboard.D),
				r: game.input.keyboard.addKey(Phaser.Keyboard.R)};

		g.fansSfx.volume = 0.7;
	  	g.fansSfx.loop = false;

	  	g.detectedSfx.volume = 0.15;
	 	g.detectedSfx.loop = false;

	 	g.successSfx.volume = 0.5;
		g.successSfx.loop = false;

		g.birdsSfx.volume = 0.3;
		g.birdsSfx.loop = true;

		g.obtainSfx.volume = 0.6;

		g.music.volume = 1.0;

		g.detected = -1;
	},


	// format in level_data for the path is:
	// speed_to_tile, tile_x, tile_y
	enemyWalkPath:function(playerDetected){
		for (var i=0; i<g.enemies.length; ++i){
	    	g.enemies[i].body.velocity.x = 0;
	    	g.enemies[i].body.velocity.y = 0;

		    var arr = g.levelData.enemy_paths[i];

			// goal position
			// 3 = stride into the path array
		    var goalPt = new Phaser.Point(g.tileSize*(arr[3*g.enemyNextTiles[i]+1]+0.5),
		    							  g.tileSize*(arr[3*g.enemyNextTiles[i]+2]+0.5));
			// vector from player position to goal position
		    var distanceVec = new Phaser.Point(goalPt.x-g.enemies[i].position.x, goalPt.y-g.enemies[i].position.y);

		    // angle to target
		    var targetPhi = Math.atan2(distanceVec.y, distanceVec.x);

		    // atan gives angle to x axis but our sprite is aligned to y axis so +90
		    var targetPhiDeg = 90.0 + targetPhi*180.0/Math.PI;
		    var ang = g.enemies[i].angle;

		    // steer enemy
			// steer right or left depending on where it is closer
			// 0.1 steering rate
		    if (ang>targetPhiDeg){
		    	if ((ang-targetPhiDeg) > (180-ang+targetPhiDeg+180)){
		    		ang += 0.1*(360-ang+targetPhiDeg);
		    	} else {
		    		ang -= 0.1*(ang-targetPhiDeg);
		    	}
		    } else {
		    	if ((targetPhiDeg-ang) > (180-targetPhiDeg+ang+180)){
		    		ang -= 0.1*(360-targetPhiDeg+ang);
		    	} else {
		    		ang += 0.1*(targetPhiDeg-ang);
		    	}
		    }
		    g.enemies[i].angle = ang;

		    // adjust speed for the next tile
			var speed = arr[3*g.enemyNextTiles[i]]*g.tileSize/64;
			// move along the computed angle
		    g.enemies[i].body.velocity.x += speed*Math.cos(targetPhi);
		    g.enemies[i].body.velocity.y += speed*Math.sin(targetPhi);

			// goal reach check (at most sqrt(2) units away)
		    if (distanceVec.getMagnitudeSq()<2) g.enemyNextTiles[i]++;

			// loop back path
			// remember, stride = 3
		    if (g.enemyNextTiles[i]==arr.length/3) g.enemyNextTiles[i] = 0;
	    }
	},


	handleKeyboard:function(playerDetected){	
		var up = g.cursors.up.isDown || g.wasdr.w.isDown;
		var down = g.cursors.down.isDown || g.wasdr.s.isDown;
		var left = g.cursors.left.isDown || g.wasdr.a.isDown;
		var right = g.cursors.right.isDown || g.wasdr.d.isDown;

		var speed = 100*g.tileSize/64;
		// speed/sqrt(2) when going diagonally
		var speedAdjusted = speed/1.414213;

		g.player.body.velocity.x = 0;
		g.player.body.velocity.y = 0;

		// player detected, act as if the player presses nothing
		if (playerDetected>-1){
			up = false;
			down = false;
			left = false;
			right = false;
		}

	    if (left) {
	    	g.stepSfx.play('', 0, 1, false, false);
	    	if (up){
				g.player.body.velocity.x = -speedAdjusted;
				g.player.body.velocity.y = -speedAdjusted;
	    		g.player.angle = -45;
	    	} 
	    	else if (down){
				g.player.body.velocity.x = -speedAdjusted;
				g.player.body.velocity.y = speedAdjusted;
	    		g.player.angle = -135;
	    	}
	    	else{
	    		g.player.body.velocity.x = -speed;
	    		g.player.angle = -90;
	    	}
	    }
	    else if (right) {
	    	g.stepSfx.play('', 0, 1, false, false);
	    	if (up){
		    	g.player.body.velocity.x = speedAdjusted;
		    	g.player.body.velocity.y = -speedAdjusted;
		       	g.player.angle = 45;
	    	} 
	    	else if (down){
				g.player.body.velocity.x = speedAdjusted;
				g.player.body.velocity.y = speedAdjusted;
	    		g.player.angle = 135;
	    	}
	    	else{
	    		g.player.body.velocity.x = speed;
	    		g.player.angle = 90;
	    	}
	    }
	    else if (down) {
	    	g.stepSfx.play('', 0, 1, false, false);
	    	g.player.body.velocity.y = speed;
	    	g.player.angle = 180;
	    }
	    else if (up) {
	    	g.stepSfx.play('', 0, 1, false, false);
	    	g.player.body.velocity.y = -speed;
	    	g.player.angle = 0;
	    }
	    else {
			g.player.body.velocity.x = 0;
			g.player.body.velocity.y = 0;
	    	g.player.frame = g.gotDocs ? 7 : 2;
	    }
	},


	ptDistSq: function(a, b){
		return ( (b.y-a.y)*(b.y-a.y) + (b.x-a.x)*(b.x-a.x) );
	},


	// the shadowmask is a black polygon on a white background
	// where it is black, we consider it the visible region by the enemy
	// algorithm to compute the polygon:
	//
	// 1.    for every enemy
	// 2.        for every wall segment i
	// 3.            cast ray1 from the enemy position towards the start point of the wall segment
	// 4.            cast ray2 very slightly left of ray0 (so as to hit any wall behind)
	// 5.            cast ray3 very slightly right of ray0 (so as to hit any wall behind)
	// 6.            for every other wall segment j
	// 7.                find intersection points of rays 1,2,3 with j
	// 8.                keep the closest one for each ray and add it to the polygon
	// 9.                sort polygon clockwise so it can be connected correctly
	//
	// loops: n*(n-1)*m where n = number of wall segments and m = number of enemies 
	// so this algorithm is more than O(n^2) and could be optimized a lot
	// by spatial partitioning (ignoring impossible intersections etc.)

	createShadowMask: function(){
		// 1.
		for (var k=0; k<g.enemies.length; k++){
			// intersection points of the rays with the wall segments
			var ps=[];
			// 2.
			for (var i=0; i<g.wallSegments.length; i++){
				var pos = g.enemies[k].position;
				// 3. make the rays long enough to cover the whole canvas
				// we don't need the end point as it will be the starting point of another wall segment
				var a = Math.atan2(g.wallSegments[i].start.y-pos.y, g.wallSegments[i].start.x-pos.x);
				var ray1 = new Phaser.Line(pos.x, pos.y, pos.x+2*resX*Math.cos(a), pos.y+2*resY*Math.sin(a));
				// 4. another 0.0001 radians left
				a+=0.0001;
				var ray2 = new Phaser.Line(pos.x, pos.y, pos.x+2*resX*Math.cos(a), pos.y+2*resY*Math.sin(a));
				// 5. another 0.0001 radians right
				a-=0.0002;
				var ray3 = new Phaser.Line(pos.x, pos.y, pos.x+2*resX*Math.cos(a), pos.y+2*resY*Math.sin(a));

				// distance from enemy to intersection for each ray
				var dis1=3*resX*resX;
				var dis2=3*resX*resX;
				var dis3=3*resX*resX;
				// intersection point closet to the enemy for each ray
				var cp1;
				var cp2;
				var cp3;

				// 6.
				// there will always be an intersection (at worst, the level's boundaries)
				for (var j=0; j<g.wallSegments.length; j++){
					var p1 = ray1.intersects(g.wallSegments[j]);
					var p2 = ray2.intersects(g.wallSegments[j]);
					var p3 = ray3.intersects(g.wallSegments[j]);
					// if a point was found and the distance from the enemy
					// is less than the current max take this point as the closest
					if (p1!=null && this.ptDistSq(pos,p1)<dis1){
						cp1={x:p1.x, y:p1.y};
						dis1 = this.ptDistSq(pos,p1);
					}
					// same for ray2
					if (p2!=null && this.ptDistSq(pos,p2)<dis2){
						cp2={x:p2.x, y:p2.y};
						dis2 = this.ptDistSq(pos,p2);
					}
					//same for ray3
					if (p3!=null && this.ptDistSq(pos,p3)<dis3){
						cp3={x:p3.x, y:p3.y};
						dis3 = this.ptDistSq(pos,p3);
					}
				}
				// we now found the intersection points for all 3 rays
				ps.push(cp1);
				ps.push(cp2);
				ps.push(cp3);
			}

			// sort clockwise by angle
			ps.sort(function(a,b){
				return (Math.atan2(b.y-pos.y, b.x-pos.x) - Math.atan2(a.y-pos.y, a.x-pos.x));
			});

			// initialize black polygon
			// it will act as the shadow mask
			g.fovGfx[k].clear();
			g.fovGfx[k].lineStyle(1,0xffffff,1);
			g.fovGfx[k].moveTo(0,0);
			g.fovGfx[k].lineTo(512,0);
			g.fovGfx[k].lineTo(512,512);

			// draw polygon from the intersection points
			g.fovGfx[k].beginFill(0xffffff);
			g.fovGfx[k].moveTo(ps[0].x,ps[0].y);
			for (var i=0; i<ps.length; i++){
				// invert y-coord because canvas coords start top left
				g.fovGfx[k].lineTo(ps[i].x, 512-ps[i].y);
			}

			g.fovGfx[k].endFill();
			// cache the bitmap. We will apply some filters on this and add the sprite to the world
			g.fovGfx[k].cacheAsBitmap = true;
		}
	},


	ToRad: function(a){
		return 3.141592*a/180.0;
	},


	dotPr: function(a,b){
		return (a.x*b.x + a.y*b.y);
	},

	// check if player is visible
	// algorithm:
	// 1.    for every enemy
	// 2.        if the player is out of enemy fov limits
	// 3.            cancel the raycast and continue to next enemy
	// 4.        else
	// 5.            cast ray from player to enemy
	// 6.            for every line segment
	// 7.                if it intersects the ray
	// 8.                    player is not visible, skip all other walls and go to next enemy
	// 9.                else
	// 10.                   go to next line segment
	// 11.           if no intersection points were found 
	// 12.               player is visible  

	raycastToPlayer: function() {
		var tempDet = -1;

		// if g.detected is >-1 then a detection has already happened the previous frame
		// so don't continue checking for visibility
		if (g.detected > -1) return g.detected;

		// 1.
		for (var i=0; i<g.enemies.length; i++){
			// not detected initially
			tempDet = -1;
			g.enFXSpr[i].filters[0].uniforms.detected.value = -1;

			var pos = g.enemies[i].position;
			
			// if the player touches an enemy, they lose
			// return the index of the enemy that the player touched
			// i >=0 so the shader will go into detection mode (check enemy_fov_filter.c)
			if (game.physics.arcade.overlap(g.enemies[i], g.player)){
				tempDet = i;
				g.enFXSpr[i].filters[0].uniforms.detected.value = tempDet;
				return tempDet;
			}

			// 2.
			var fov = g.levelData.enemy_effects[i][0];
			var dis = g.levelData.enemy_effects[i][1];
			// segment from player to enemy
			var p = new Phaser.Point(g.player.position.x-pos.x, g.player.position.y-pos.y);
			// segment from player to left edge point of enemy's fov
			var p1 = new Phaser.Point(Math.cos(this.ToRad(g.enemies[i].angle+fov)), Math.sin(this.ToRad(g.enemies[i].angle+fov)));
			// segment from player to right edge point of enemy's fov
			var p2 = new Phaser.Point(Math.cos(this.ToRad(g.enemies[i].angle-fov)), Math.sin(this.ToRad(g.enemies[i].angle-fov)));
			// 3.
			if (this.ptDistSq(pos, g.player.position)>dis*dis) continue;
			if (this.dotPr(p, p1)>0.0 || this.dotPr(p, p2)<0.0) continue;

			// 4.
			// 5.
			// ray from enemy to player
			var ray = new Phaser.Line(pos.x, pos.y, g.player.position.x, g.player.position.y);
			var p = new Phaser.Point();
			// 6.
			for (var j=0; j<g.wallSegments.length; j++){
				// 7.
				p = ray.intersects(g.wallSegments[j]);
				// 8.
				if (p != null) break;
			}

			// 9.
			// if no wall found between enemy and player
			// register the id of the detecting enemy and update the filter
			if (p == null){
				tempDet = i;
				g.enFXSpr[i].filters[0].uniforms.detected.value = tempDet;
				// one detection is enough, leave the enemy loop
				break;
			}			
		}

		return tempDet;
	},


	update: function() {
		if (g.wasdr.r.isDown) game.state.restart();
		
		// fade out music on last level
		if (g.music.volume>0 && g.currLevel==24) g.music.volume -= 0.0025;

		// only play the fans sound effect when there is at least 1 fan. Currently that's always
		if (g.levelData.fans.length>0 && g.fansSfx.isPlaying==false) g.fansSfx.play();
		// only play the bird sound effect on last level
	    if (g.currLevel==24 && g.birdsSfx.isPlaying==false) g.birdsSfx.play();

	    // documents retrieval 
	    if (game.physics.arcade.overlap(g.player, g.docsSprite)){
	    	g.docsSprite.kill();
	    	g.obtainSfx.play();
	    	g.gotDocs = true;
	    }

	    // player at exit when at least 8 units close  
	    g.atExit = (this.ptDistSq(g.player.body.position, g.exitPos)<64) ? true : false;

	    // enemy detected player
	    g.detected = this.raycastToPlayer();

	    // only allow keyboard control when the player is not: 1)detected 2)at the exit with the docs
	    if ( !(g.atExit&&g.gotDocs) ) this.handleKeyboard(g.detected);

		// advance enemy positions
	    this.enemyWalkPath(g.detected);

	    this.createShadowMask();

	    // general collisions
	    var hitPlatform = game.physics.arcade.collide(g.player, g.wallsGroup);
	    hitPlatform = game.physics.arcade.collide(g.enemyGroup, g.wallsGroup);
	    hitPlatform = game.physics.arcade.collide(g.player, g.enemyGroup);

	    // handle state and enemy animation cases (player detected or not)
	    if (g.detected>-1){
			if (g.detectedSfx.isPlaying == false) g.detectedSfx.play();

			// start the fading effect
	    	g.fadeTime++;
	    	g.fadeBlackFx.uniforms.fade_time.value = g.fadeTime;

			// player cannot move
			g.player.body.velocity.x = 0;
			g.player.body.velocity.y = 0;
			
			// enemies stop moving and animation freezes to frame 5
			for (var i=0; i<g.enemies.length; i++){
				g.enemies[i].body.velocity.x = 0;
				g.enemies[i].body.velocity.y = 0;
				g.enemies[i].frame = 5;
	    	}
			// turn the detecting enemy to face the player
			g.enemies[g.detected].angle = 90+57.3*Math.atan2((g.player.position.y-g.enemies[g.detected].position.y),
												(g.player.position.x-g.enemies[g.detected].position.x));

			// stop the fading effect and sounds and leave
	    	if (g.fadeTime > 150) {
				g.detectedSfx.volume = 0.0;
	    		g.fansSfx.stop();
	    		g.birdsSfx.stop();
	    		if (g.currLevel == 24){
					game.world.removeAll();
	    			game.state.start('end');
	    		} else {
					game.world.removeAll();
		    		game.state.start('menu');
	    		}
	    	}
		} else {
			// set animation speed for enemies
	    	for (var i=0; i<g.enemies.length; i++){
				// get speed from level data
				var currSpeed = g.levelData.enemy_paths[i][3*g.enemyNextTiles[i]];
				var anim = g.enemies[i].animations.getAnimation('anim_enemy');
	    		// 1 fps per 3 movement speed
				if (currSpeed > 1) anim.speed = currSpeed/3.0;
				// stop animating if enemy path stops
				else g.enemies[i].frame = 2;
	    	}
		}


		// update enemy radar effects uniforms
	    for (var i=0; i<g.enemies.length; i++){
	    	g.enFXSpr[i].filters[0].uniforms.iChannel0.value = g.fovGfx[i]._cachedSprite.texture;
		    g.enFXSpr[i].filters[0].uniforms.enemyPos.value = g.enemies[i].position;
		    g.enFXSpr[i].filters[0].uniforms.enemyAngle.value = g.enemies[i].angle;
		    g.enFXSpr[i].filters[0].uniforms.enemyFov.value = g.levelData.enemy_effects[i][0];
		    g.enFXSpr[i].filters[0].uniforms.enemyDist.value = g.levelData.enemy_effects[i][1];
			if (g.detected>-1) g.enFXSpr[g.detected].filters[0].uniforms.detected.value = g.detected;
		    g.enFXSpr[i].filters[0].update();
	    }
	    g.docsSprite.filters[0].update();


	    // handle state and the different player animation depending on mission state
	    if (g.gotDocs) {
		    if (g.atExit){
				// start fading to white
		    	g.fadeTime--;
	    		g.fadeBlackFx.uniforms.fade_time.value = g.fadeTime;

				// re-adjust player position exactly above exit position
				// and play the exit animation
		    	g.player.body.position.x = g.exitPos.x;
		    	g.player.body.position.y = g.exitPos.y;
		    	g.player.animations.play('anim_exit', 5, false, true);

		    	if (g.fadeTime < -150) {
					// stop fading, stop sounds
		    		g.fadeTime = 0;
	    			g.fadeBlackFx.uniforms.fade_time.value = g.fadeTime;
					g.successSfx.volume = 0;
	    			g.fansSfx.stop();
	    			g.birdsSfx.stop();
					// advance a level
	    			if (g.currLevel >= g.levelReached) g.levelReached++;
	    			if (g.levelReached > 23) g.levelReached = 23;
	    			//localStorage.setItem('infiltrator_level_progress', JSON.stringify(g.levelReached));
		    		if (g.currLevel < 23) {
						// on levels 0-22, go back to menu
						game.world.removeAll();
		    			game.state.start('menu');
		    		} else {
						// on level 23, immediately go to secret level 24
		    			g.currLevel = 24;
						game.world.removeAll();
		    			game.state.restart();
		    		}
		    	}
		    } else {
	    		g.player.animations.play('anim_with_docs');
		    }
	    } else {
	    	g.player.animations.play('anim');
	    }
	    
	    // rotating fans
	    for (var i=0; i<g.fans.length; i++){
	        g.fans[i].angle += 2*Math.random()+2;
	    }
	}
}

var endState = {
	// this is almost the same with the intro sequence
	create: function() {
		g.fadeTime = 0;
	    g.fadeBlackFx.uniforms.fade_time.value = g.fadeTime;

		g.menuPlayerImg = game.add.sprite(256, 256, 'player');
		g.menuPlayerImg.anchor.setTo(0.5, 0.5);
	    g.menuPlayerImg.scale.setTo(0.5, 0.5);
	    g.menuPlayerImg.angle = 90.0;
		g.menuPlayerImg.animations.add('anim_with_docs', [5, 6, 7, 8, 9], 15, true);
	    g.menuPlayerImg.animations.add('anim_exit', [10, 11, 12, 13, 14], 15, true);
		g.menuPlayerImg.animations.play('anim_with_docs', 5, true, false);

		g.logoSpr = game.add.sprite(256, -128, 'the_end_img');
		g.logoSpr.anchor.x = 0.5;
	 	g.logoSpr.anchor.y = 0.5;
	 	g.logoVel = 0;
	 	g.playSpr = game.add.sprite(256, -128, 'play_again_img');
	 	g.playSpr.anchor.x = 0.5;
	 	g.playSpr.anchor.y = 0.5;
	 	g.playVel = 0;

		g.audioButton = game.add.sprite(0, 0, 'audio_toggle');
		g.audioButton.frame = game.sound.mute ? 1 : 0;
		g.audioButton.alpha = 0.4;
	    g.audioButton.inputEnabled = true;
		g.audioButton.events.onInputOver.add(function(){g.audioButton.alpha = 0.8;}, this);
		g.audioButton.events.onInputOut.add(function(){g.audioButton.alpha = 0.4;}, this);
		g.audioButton.events.onInputDown.add(function(){
			game.sound.mute = !game.sound.mute;
			g.audioButton.frame = game.sound.mute ? 1 : 0;
		}, this);
	},

	update: function() {
		//restore music volume
		if (g.music.volume < 1) g.music.volume += 0.05;
		
		// same with the intro sequence
		g.logoVel += 0.1;
		g.logoSpr.y += g.logoVel;
		g.playVel += 0.1;
		g.playSpr.y += g.playVel;

		g.fadeTime += 0.01;
		if (g.logoSpr.scale.y < 1) g.logoSpr.scale.y += 0.05;
		if (g.logoSpr.y > 192) {
			if (g.logoVel<0.2) g.logoVel = 0;
			g.logoSpr.scale.y -= 0.07*g.logoVel;
			var vol = g.logoVel*g.logoVel/10;
			vol = vol > 0.6 ? 0.6 : vol;
			g.bounceSfx.play('', 0, vol, false, true);
			g.logoVel *= -0.5;
			g.logoSpr.y = 192;
		}
		if (g.playSpr.scale.y < 1) g.playSpr.scale.y += 0.05;
		if (g.playSpr.y > 320){
			if (g.playVel<0.2) g.playVel = 0;
			g.playSpr.scale.y -= 0.07*g.playVel;
			var vol2 = g.playVel*g.playVel/10;
			vol2 = vol2 > 0.6 ? 0.6 : vol2;
			g.bounceSfx2.play('', 0, vol2, false, true);
			g.playVel *= -0.5;
			g.playSpr.y = 320;
		}
		if (g.fadeTime > 2.5) {
			g.menuPlayerImg.animations.play('anim_exit', 5, false, true);

			g.playSpr.inputEnabled = true;
			g.playSpr.events.onInputOver.add(this.SprScaleUp, this, 0, g.playSpr);
			g.playSpr.events.onInputOut.add(this.SprScaleDown, this, 0, g.playSpr);
			g.playSpr.events.onInputDown.addOnce(function()
				{
					game.world.filters = [];
					game.world.removeAll();
					game.state.start('menuAnim');
				}, this);
		}
	},

	SprScaleUp: function(spr){
		spr.scale.x = 1.1;
		spr.scale.y = 1.1;
	},
	SprScaleDown: function(spr){
		spr.scale.x = 1.0;
		spr.scale.y = 1.0;
	},
}

game.state.add('load', loadState);
game.state.add('menuAnim', menuAnimState);
game.state.add('menu', menuState);
game.state.add('game', gameState);
game.state.add('end', endState);
game.state.start('load');