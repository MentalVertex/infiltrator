precision mediump float;

varying vec2 vTextureCoord;
uniform sampler2D uSampler;
uniform float fade_time;

// this filter used for the blur effect during the intro sequence
// there is also some fade to black applied
// this filter is applied many times on itself (see infiltrator.js renderTextures)

void main(void) {
	float coef = 0.001*fade_time;

	// typical 3x3 blur kernel
	// the window spreads as the time goes by
	vec4 n  = texture2D(uSampler, vec2(vTextureCoord.x,        vTextureCoord.y + coef));
	vec4 ne = texture2D(uSampler, vec2(vTextureCoord.x + coef, vTextureCoord.y + coef));
	vec4 e  = texture2D(uSampler, vec2(vTextureCoord.x + coef, vTextureCoord.y));
	vec4 se = texture2D(uSampler, vec2(vTextureCoord.x + coef, vTextureCoord.y - coef));
	vec4 s  = texture2D(uSampler, vec2(vTextureCoord.x,        vTextureCoord.y - coef));
	vec4 sw = texture2D(uSampler, vec2(vTextureCoord.x - coef, vTextureCoord.y - coef));
	vec4 w  = texture2D(uSampler, vec2(vTextureCoord.x - coef, vTextureCoord.y));
	vec4 nw = texture2D(uSampler, vec2(vTextureCoord.x - coef, vTextureCoord.y + coef));

	gl_FragColor = texture2D(uSampler, vTextureCoord);
	// average of the neighbours' colors
	gl_FragColor.x = 0.125*(n.x + ne.x + e.x + se.x + s.x + sw.x + w.x + nw.x);
	gl_FragColor.y = 0.125*(n.y + ne.y + e.y + se.y + s.y + sw.y + w.y + nw.y);
	gl_FragColor.z = 0.125*(n.z + ne.z + e.z + se.z + s.z + sw.z + w.z + nw.z);
	// darken the colors a bit every time
	gl_FragColor -= 0.0035;
}