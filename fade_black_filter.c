precision mediump float;

varying vec2 vTextureCoord;
uniform sampler2D uSampler;
uniform float fade_time;
uniform float darkness;

// simple fade to black/white filter
// just decrease/increase the color as the time goes by
// we pass negative time to fade to white (see infiltrator.js)

// also here we apply the level darkness

void main(void) {
	float coef = 0.01*fade_time;

	gl_FragColor = texture2D(uSampler, vTextureCoord);
	gl_FragColor -= darkness;
	gl_FragColor -= coef;
}