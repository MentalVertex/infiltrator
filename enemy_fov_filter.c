precision mediump float;

uniform float time;
varying vec2 vTextureCoord;
uniform sampler2D uSampler;
uniform vec2 resolution;
uniform vec2 enemyPos;
uniform float enemyAngle;
uniform float enemyFov;
uniform float enemyDist;
uniform sampler2D iChannel0;
uniform float detected;

// this filter is applied on the enFxSpr's for the green/red fov effect
// there is one such sprite for every enemy, each covering the whole window

float toRad(float degrees){ return -3.141592*(degrees)/180.0;}

void main(void) {
	vec4 texColor = texture2D(uSampler, vTextureCoord);
	// iChannel0 carries the bitmapData of the shadowMask
	// black for visible regions, white for invisible regions
	vec4 shadowMask = texture2D(iChannel0, vTextureCoord);

	vec2 adjustedPos = vec2(enemyPos.x, resolution.y-enemyPos.y);

	// these 2 points together with dis define the circular sector of the fov
	vec2 p1 = vec2(cos(toRad(enemyAngle+enemyFov)), sin(toRad(enemyAngle+enemyFov)));
	vec2 p2 = vec2(cos(toRad(enemyAngle-enemyFov)), sin(toRad(enemyAngle-enemyFov)));
	vec2 dis = vec2(resolution.x*vTextureCoord.x, resolution.y*vTextureCoord.y)-adjustedPos;

	// if not detected (== -1) then the R coefficient will be 0 (green fov)
	// if detected (> -1) then G will be 0 (red fov)
	float detCoef = 0.0;
	if (detected>-0.5) detCoef = 0.3;

	// if the pixel is 1) in the shadow mask 2) close enough to be in the fov
	// and 3) it's between the sides of the fov then it's actually in the fov
	// 20.0 is some slight offset so that the effect does not start exactly in front of the enemy
  	if (shadowMask.x>0.99 &&  length(dis)>20.0 && length(dis)<enemyDist && dot(p1, dis)<0.0 && dot(p2, dis)>0.0){
		// magic - adjustable by the numbers
  		float coef = pow(cos(1.0*(toRad(enemyAngle)+atan(dis.y/dis.x))-(1.0+30.0*detCoef)*time), 2.0);
		// swap colors depending on detected
		texColor += vec4(detCoef*coef, (0.3-detCoef)*coef, 0.0, 0.0);
    }

    gl_FragColor = texColor;

}